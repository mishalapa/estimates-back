// const Poll = require("pg").Pool;
// const pool = new Poll({
//   user: "postgres",
//   database: "node_postgres",
//   password: "9133961",
//   host: "localhost",
//   port: 5432,
// });

// module.exports = pool;

const { Sequelize } = require("sequelize");

module.exports = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    dialect: "postgres",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
  }
);
