const jwt = require("jsonwebtoken");
const tokenModel = require("../models/user_model");
class TokenService {
  generateTokens(payload) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {
      expiresIn: "15m",
    });
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {
      expiresIn: "30d",
    });
    return {
      accessToken,
      refreshToken,
    };
  }
  validateAccessToken(token) {
    try {
      const userData = jwt.verify(token, process.env.JWT_ACCESS_SECRET);
      return userData;
    } catch (e) {
      return null;
    }
  }
  validateRefreshToken(token) {
    try {
      const userData = jwt.verify(token, process.env.JWT_REFRESH_SECRET);
      return userData;
    } catch (e) {
      return null;
    }
  }

  async saveToken(userId, refreshToken) {
    const tokenData = await tokenModel.Token.findOne({
      where: { userId: userId },
    });
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      tokenData.userId = userId;
      return tokenData.save();
    }
    const token = await tokenModel.Token.create({
      userId: userId,
      refreshToken,
    });
    return token;
  }
  async removeToken(refreshToken) {
    const tokenData = await tokenModel.Token.findOne({
      where: { refreshToken },
    });
    await tokenData.destroy();
    return tokenData;
  }

  async findToken(refreshToken) {
    const tokenData = await tokenModel.Token.findOne({
      where: { refreshToken },
    });
    return tokenData;
  }
}

module.exports = new TokenService();
