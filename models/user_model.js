const sequelize = require("../db");
const { DataTypes, UUID } = require("sequelize");
const { UUIDV4 } = require("sequelize");

const User = sequelize.define("user", {
  id: { type: DataTypes.UUID, primaryKey: true, defaultValue: UUIDV4 },
  email: { type: DataTypes.STRING, unique: true, allowNull: false },
  password: { type: DataTypes.STRING, allowNull: false },
  isActivated: { type: DataTypes.BOOLEAN, defaultValue: false },
  activationLink: { type: DataTypes.STRING },
});

const Token = sequelize.define("token", {
  id: { type: DataTypes.UUID, primaryKey: true, defaultValue: UUIDV4 },
  refreshToken: { type: DataTypes.STRING(500), allowNull: false },
});

User.hasOne(Token, {
  foreignKey: {
    type: DataTypes.UUID,
    allowNull: false,
  },
});
Token.belongsTo(User);

sequelize.sync({ alter: true });

module.exports = {
  User,
  Token,
};
